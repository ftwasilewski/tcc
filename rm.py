#!/bin/bash/python

import socket
import sys
import logging
import traceback

# Server port
PORT = 30208
BUFFERSIZE = 10240

# Usage:
# action ip_host ip_source_vm ip_destination_vm allocation_size type

# Log parameters
LOG_FILENAME = "rm.log"
LOG_FILEMODE = "a"
LOG_LEVEL = logging.INFO
LOG_FORMAT = "%(asctime)s [%(levelname)-8s]: %(message)s"


# set up logging to file
logging.basicConfig(level=logging.DEBUG,
                    format=LOG_FORMAT,
                    filename=LOG_FILENAME,
                    filemode=LOG_FILEMODE)

# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(LOG_LEVEL)
# set a format for console use
formatter = logging.Formatter(LOG_FORMAT)
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)


class CustomError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


def check_request(request):
    # TODO: Check the request
    logging.info("Request: " + str(request))

    # If something is wrong:
    #     raise CustomError("Something is wrong with your request format")


if __name__ == "__main__":
    try:
        # Check the request
        check_request(sys.argv)

        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connect the socket to Configurator socket
        sock.connect((sys.argv[2], PORT))

        # Send request
        sock.sendall(' '.join(sys.argv))

        # Receive response
        response = sock.recv(BUFFERSIZE)

        # If no response received
        if not response:
            sock.close()

        logging.info("Response: " + response)

    except CustomError:
        logging.error(sys.exc_value)
        logging.debug(traceback.format_exc())

    except Exception:
        # logging.error(sys.exc_value)
        logging.error("Something went wrong. See " + LOG_FILENAME + " file for more information")
        logging.debug(traceback.format_exc())

    finally:
        try:
            sock.close()
        except NameError:
            logging.error(sys.exc_value)