#!/bin/bash/python
# coding=utf-8

import socket
import sys
import subprocess
import re
import traceback
import os
import logging

PORT = 30208
PORTREPD = 8080
BUFFERSIZE = 10240
VIFCAPACITY = 810000  # kbits (1gbit = 1000mbit = 1000000kbit)
PHYCAPACITY = 0 # is defined when the program is executed
PHYSICALINTERFACE = "p10p1"
LANG = "" # This is setted inside main
# RESERVABLE_QUOTA = 1 # This is the % of BW that will be available to reserve, for each interface
LSHW = "" # This is setted iside main

# TODO: Lidar com o RESERVABLE_QUOTA
# TODO: Criar classes Filter (handle, flowid, ip_src, ip_dst), Class (id, rate), etc
# TODO: Modificar as saídas stdout para log
# TODO: Permitir modificacao do level do log ao iniciar o config.py
# TODO: Analisar possível substituição do "arp -a" por "arping"

# Log parameters
LOG_FILENAME = "config.log"
LOG_FILEMODE = "w"
LOG_LEVEL = logging.DEBUG
LOG_FORMAT = "%(asctime)s [%(levelname)-8s]: %(message)s"


# set up logging to file
logging.basicConfig(level=logging.DEBUG,
                    format=LOG_FORMAT,
                    filename=LOG_FILENAME,
                    filemode=LOG_FILEMODE)

# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(LOG_LEVEL)
# set a format for console use
formatter = logging.Formatter(LOG_FORMAT)
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)


class Request():
    def __init__(self, action="unknow", ip_host="unknow", ip_source_vm="unknow",
                 ip_destination_vm="unknow", allocation_size="unknow", type="unknow"):
        self.action = action
        self.ip_host = ip_host  # Only used in Request Manager
        self.ip_source_vm = ip_source_vm
        self.ip_destination_vm = ip_destination_vm
        self.allocation_size = allocation_size
        self.type = type

    def __str__(self):
        return "action: %s \nip_host: %s \nip_source_vm: %s \nip_destination_vm: %s \nallocation_size: %s \ntype: %s" %\
               (self.action, self.ip_host, self.ip_source_vm, self.ip_destination_vm, self.allocation_size, self.type)


class CustomError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg

# TODO: def ip_to_vif()

def ip_to_hex(dec):
    hex = dec.split('.')
    return '{:02X}{:02X}{:02X}{:02X}'.format(*map(int, hex))


def hex_to_ip(hex):
    bytes = ["".join(x) for x in zip(*[iter(hex)]*2)]
    bytes = [int(x, 16) for x in bytes]
    return ".".join(str(x) for x in reversed(bytes))


def ip_to_mac(ip):
    # When not using static arp, uncomment these lines
    # p1 = subprocess.Popen(["ping", ip, "-c", "3"], stdout=subprocess.PIPE)
    # p1_output = p1.stdout.read()
    # if re.search("100% packet loss", p1_output):
    #     raise CustomError("Destination Virtual Machine (IP: " + ip + ") Unreachable From Host")
    p1 = subprocess.Popen(["arp", "-a", ip], stdout=subprocess.PIPE)
    p1_output = p1.stdout.read()
    if LANG == "pt":
        re_result = re.search("em\s(\S*)", p1_output)
    if LANG == "en":
        re_result = re.search("at\s(\S*)", p1_output)
    return re_result.group(1)


def vif_quantity():
    # Return the number of vif's
    p1 = subprocess.Popen(["ip", "link"], stdout=subprocess.PIPE)
    p1_output = p1.stdout.read()
    return p1_output.count("vif")


def vifs():
    # Return a list all vifs in the system
    p1 = subprocess.Popen(["ip", "link"], stdout=subprocess.PIPE)
    p1_output = p1.stdout.read()
    re_result = re.findall("\s{1}(vif[0-9]*\.[0-9]*)", p1_output)
    return re_result


def create_request(data):
    d = data.split()
    if d[1] == "allocate":
        return Request(d[1], "not used", d[3], d[4], d[5], d[6])
    elif d[1] == "deallocate":
        return Request(d[1], "not used", d[3], d[4], "not used", "not used")
    elif d[1] == "clearall":
        return Request(d[1], "not used", "not used", "not used", "not used", "not used")
    elif d[1] == "check":
        return Request(d[1], "not used", "not used", "not used", "not used", "not used")
    else:
        raise CustomError("Nonexistent action")


def del_qdisc(interface):
    p = subprocess.Popen(["tc", "qdisc", "del", "dev", interface, "root"], stdout=subprocess.PIPE)
    p_output = p.stdout.read()
    logging.debug(p_output)


# Always return "kbit" format
def get_network_capacity(interface):
    if interface == PHYSICALINTERFACE:
        return PHYCAPACITY
        # p1 = subprocess.Popen(["lshw", "-C", "network"], stdout=subprocess.PIPE)
        # #p1 = LSHW
        # # The default language can be other (e.g., 'capacity' or 'capacidade')
        # if LANG == "en":
        #     p2 = subprocess.Popen(["grep", "capacity"], stdin=p1.stdout, stdout=subprocess.PIPE)
        # if LANG == "pt":
        #     p2 = subprocess.Popen(["grep", "capacidade"], stdin=p1.stdout, stdout=subprocess.PIPE)
        # p2_output = p2.stdout.read()
        # print p2_output
        # nc = p2_output[p2_output.index(":")+2:p2_output.index("/")]
        # if re.match("[0-9]*Gbit", nc):
        #     m = re.search("[0-9]*", nc)
        #     nc = str(int(m.group())*1000000)
        #     # nc = str(nc) + "".join("kbit")
        # elif re.match("[0-9]*Mbit", nc):
        #     m = re.search("[0-9]*", nc)
        #     nc = str(int(m.group())*1000)
        #     # nc = str(nc) + "".join("kbit")
        # return nc
    elif re.search("(vif)", interface):  # TODO: (não será mais necessário) deverá retornar (VIFCAPACITY - TOTAL DE ALOCACOES NAS VIFS)
        return str(VIFCAPACITY/vif_quantity())
    return 0

def get_phy_network_capacity():
    p1 = subprocess.Popen(["lshw", "-C", "network"], stdout=subprocess.PIPE)
    #p1 = LSHW
    # The default language can be other (e.g., 'capacity' or 'capacidade')
    if LANG == "en":
        p2 = subprocess.Popen(["grep", "capacity"], stdin=p1.stdout, stdout=subprocess.PIPE)
    if LANG == "pt":
        p2 = subprocess.Popen(["grep", "capacidade"], stdin=p1.stdout, stdout=subprocess.PIPE)
    p2_output = p2.stdout.read()
    logging.debug(p2_output)
    nc = p2_output[p2_output.index(":")+2:p2_output.index("/")]
    if re.match("[0-9]*Gbit", nc):
        m = re.search("[0-9]*", nc)
        nc = str(int(m.group())*1000000)
        # nc = str(nc) + "".join("kbit")
    elif re.match("[0-9]*Mbit", nc):
        m = re.search("[0-9]*", nc)
        nc = str(int(m.group())*1000)
        # nc = str(nc) + "".join("kbit")
    logging.debug("physical capacity: " + str(nc))
    return nc

def same_host(request):

    # General destination (work just for destination outside the host)
    if request.ip_destination_vm == "0.0.0.0":
        return False, "none", "none"

    p1 = subprocess.Popen(["xenstore-ls", "/local/domain/0"], stdout=subprocess.PIPE)
    p1_output = p1.stdout.read()
    mac = ip_to_mac(request.ip_destination_vm)
    #  TODO: Verificar possivel problema com maiusculas e minusculas
    if re.search(mac, p1_output, re.IGNORECASE):
        return True, p1_output, mac
    else:
        return False, "none", "none"


def is_first_configuration(interface):
    p1 = subprocess.Popen(["ip", "link", "show", interface], stdout=subprocess.PIPE)
    p1_output = p1.stdout.read()
    return not("htb" in p1_output or "psp" in p1_output)


def filters(interface):
    # Return a list of filters (filter_id, flowid, src_ip?, dst_ip?)
    p1 = subprocess.Popen(["tc", "filter", "show", "dev", interface], stdout=subprocess.PIPE)
    p1_output = p1.stdout.read()
    re_result =re.findall("(800::[0-9]+)[\S| ]*\sflowid\s(1:[0-9]+)[\s]*\n[\s]*match\s([\w]*)/\w* at [0-9]+[\s]*\n[\s]*match\s([\w]*)/\w* at [0-9]+", p1_output)
    return re_result


def is_modification(request, interface):
    filter_list = filters(interface)
    src_ip_hex = ip_to_hex(request.ip_source_vm)
    dst_ip_hex = ip_to_hex(request.ip_destination_vm)
    for i in filter_list:
        if src_ip_hex.lower() in i and dst_ip_hex.lower() in i:
            return True, i
    return False, None


def class_rate(interface, classid):
    # Return the rate of a specific class, in kbit
    p1 = subprocess.Popen(["tc", "class", "show", "dev", interface], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(["grep", classid], stdin=p1.stdout, stdout=subprocess.PIPE)
    p2_output = p2.stdout.read()
    if not p2_output:
        raise CustomError("Classid " + classid + " not found")
    re_result = re.search("(rate\s)([0-9]*)(G|M|K)bit", p2_output)
    rate = 0
    if re_result.group(3) == "K":
        rate = int(re_result.group(2))
    elif re_result.group(3) == "M":
        rate = int(re_result.group(2))*1000
    elif re_result.group(3) == "G":
        rate = int(re_result.group(2))*1000000
    return rate


def configure_htb(request, interface="eht0"):
    logging.debug("Request allocation size: " + str(request.allocation_size))
    is_modification_return = is_modification(request, interface)
    if is_modification_return[0]:
        filter = is_modification_return[1]
        # (filter_id, flowid (classid), src_ip?, dst_ip?)
        spare_capacity = class_rate(interface, "1:10") + class_rate(interface, filter[1])
        if int(request.allocation_size) >= spare_capacity:
            raise CustomError("There is no capacity enough " + spare_capacity)
        # Modify default class (1:10) rate
        subprocess.call(["tc", "class", "change", "dev", interface, "parent", "1:1", "classid", "1:10", "htb", "rate", str(spare_capacity-int(request.allocation_size))+"kbit"])
        # Modify classid rate
        subprocess.call(["tc", "class", "change", "dev", interface, "parent", "1:1", "classid", filter[1], "htb", "rate", request.allocation_size+"kbit"])

    else:
        if is_first_configuration(interface):
            nc = get_network_capacity(interface)
            if int(request.allocation_size) > int(nc):
                raise CustomError("There is not capacity enough")
            subprocess.call(["tc", "qdisc", "add", "dev", interface, "root", "handle", "1:", "htb", "default", "10"])
            subprocess.call(["tc", "class", "add", "dev", interface, "parent", "1:", "classid", "1:1", "htb", "rate", nc+"kbit"])
            if int(request.allocation_size) != int(nc):
                subprocess.call(["tc", "class", "add", "dev", interface, "parent", "1:1", "classid", "1:10", "htb", "rate", nc+"kbit"])
                logging.debug("Is first configuration 1:10 rate: " + str(nc))
                # subprocess.call(["tc", "class", "add", "dev", interface, "parent", "1:1", "classid", "1:10", "htb", "rate", str(int(nc)-int(request.allocation_size))+"kbit"])
            # subprocess.call(["tc", "class", "add", "dev", interface, "parent", "1:1", "classid", "1:11", "htb", "rate", request.allocation_size+"kbit"])
            # subprocess.call(["tc", "filter", "add", "dev", interface, "protocol", "ip", "parent", "1:0", "prio", "1", "u32",
            #                  "match", "ip", "src", request.ip_source_vm, "match", "ip", "dst", request.ip_destination_vm, "flowid", "1:11"])
            if "vif" in interface:
                # Configure the default qdisc for other vifs
                # TODO: criar qdisc na ingress das vifs?
                # TODO: Atenção para a necessidade ou não da retirada da interface em questão
                vif_list = vifs()
                for v in vif_list:
                    subprocess.call(["tc", "qdisc", "add", "dev", v, "root", "handle", "1:", "htb", "default", "10"])
                    subprocess.call(["tc", "class", "add", "dev", v, "parent", "1:", "classid", "1:1", "htb", "rate", nc+"kbit"])
                    subprocess.call(["tc", "class", "add", "dev", v, "parent", "1:1", "classid", "1:10", "htb", "rate", nc+"kbit"])

            # Special communication channel
            if interface == PHYSICALINTERFACE:
                # class
                subprocess.call(["tc", "class", "add", "dev", interface, "parent", "1:1", "classid", "1:11", "htb", "rate", "1000kbit"])
                # filter
                subprocess.call(["tc", "filter", "add", "dev", interface, "protocol", "ip", "parent", "1:0", "prio", "1", "u32",
                         "match", "ip", "sport", str(PORT), "0xffff", "flowid", "1:11"])
                # filter for repd
                subprocess.call(["tc", "filter", "add", "dev", interface, "protocol", "ip", "parent", "1:0", "prio", "1", "u32",
                         "match", "ip", "sport", str(PORTREPD), "0xffff", "flowid", "1:11"])

        # else:
        # Find available capacity (check classid 1:10 "rate")
        spare_capacity = class_rate(interface, "1:10")
        logging.debug("Spare capacity (1:10): " + str(spare_capacity))

        if int(request.allocation_size) >= spare_capacity:
            raise CustomError("There is no capacity enough")

        # Modify default class (1:10) rate
        subprocess.call(["tc", "class", "change", "dev", interface, "parent", "1:1", "classid", "1:10", "htb", "rate", str(spare_capacity-int(request.allocation_size))+"kbit"])

        # Find the higher classid
        p1 = subprocess.Popen(["tc", "class", "show", "dev", interface], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["grep", "parent"], stdin=p1.stdout, stdout=subprocess.PIPE)
        p2_output = p2.stdout.read()
        classes = p2_output.split("\n")
        max_classid = 10
        for c in classes:
            if c:
                re_result = re.search("1:([0-9]*)\sparent", c)
                if int(re_result.group(1)) > max_classid:
                    max_classid = int(re_result.group(1))

        # Create the new class and filter
        subprocess.call(["tc", "class", "add", "dev", interface, "parent", "1:1", "classid", "1:"+str(max_classid+1), "htb", "rate", request.allocation_size+"kbit"])
        if request.ip_destination_vm == "0.0.0.0":
            # General destination (no destination specified)
            subprocess.call(["tc", "filter", "add", "dev", interface, "protocol", "ip", "parent", "1:0", "prio", "1", "u32",
                         "match", "ip", "src", request.ip_source_vm, "flowid", "1:"+str(max_classid+1)])
        else:
            subprocess.call(["tc", "filter", "add", "dev", interface, "protocol", "ip", "parent", "1:0", "prio", "1", "u32",
                         "match", "ip", "src", request.ip_source_vm, "match", "ip", "dst", request.ip_destination_vm, "flowid", "1:"+str(max_classid+1)])


    return "OK"


# TODO: implementar configure_psp
def configure_psp(request, interface="eth0"):
    return 0


def configure_pspht(request, interface="eth0"):
    if is_first_configuration(interface):
        nc = get_network_capacity(interface)
        if int(request.allocation_size) > int(nc):
            raise CustomError("There is not capacity enough")
        subprocess.call(["tc", "qdisc", "add", "dev", interface, "root", "handle", "1:", "prio", "bands", "2", "priomap",
                         "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"])
        if int(request.allocation_size) != int(nc):
            subprocess.call(["tc", "qdisc", "add", "dev", interface, "parent", "1:1", "handle", "10:", "pspht", "rate", str(int(nc)-int(request.allocation_size))+"kbit"])
        else:  # TODO: caso a alocação tome conta de toda a capacidade da vif, qual o rate da classe default?
            subprocess.call(["tc", "qdisc", "add", "dev", interface, "parent", "1:1", "handle", "10:", "pspht", "rate", "1kbit"])
        subprocess.call(["tc", "qdisc", "add", "dev", interface, "parent", "1:2", "handle", "11:", "pspht", "rate", request.allocation_size+"kbit"])
        subprocess.call(["tc", "filter", "add", "dev", interface, "protocol", "ip", "parent", "1:", "prio", "1", "u32",
                         "match", "ip", "src", request.ip_source_vm, "match", "ip", "dst", request.ip_destination_vm, "flowid", "1:2"])
    else:
        print "None"
    return "OK"


def deallocate(request, interface="eth0"):
    # Delete filter
    filter_list = filters(interface)
    src_ip_hex = ip_to_hex(request.ip_source_vm)
    dst_ip_hex = ip_to_hex(request.ip_destination_vm)
    for i in filter_list:
        if src_ip_hex.lower() in i and dst_ip_hex.lower() in i:
            p1 = subprocess.Popen(["tc", "filter", "del", "dev", interface, "pref", "1", "handle", i[0], "u32"], stdout=subprocess.PIPE)
            class_id = i[1]
            break

    # Delete class
    if not class_id:
        CustomError("Classid not found")
    rate = class_rate(interface, class_id)
    p1 = subprocess.Popen(["tc", "class", "del", "dev", interface, "classid", class_id], stdout=subprocess.PIPE)

    # Add rate to default class
    new_rate = rate + class_rate(interface, "1:10")
    subprocess.call(["tc", "class", "change", "dev", interface, "parent", "1:1", "classid", "1:10", "htb", "rate", str(new_rate)+"kbit"])


def handle_request(request):
    response = []
    if request.action == "allocate":
        #interface = ""
        answer = same_host(request)
        if answer[0]:
            interface = []
            # if answer[0] is True, answer[1] contains the output of xenstore-ls and answer[2] contains the mac
            re_result = ""
            cont = 1
            while not re_result:
                re_result = re.findall("\n  ([0-9]*) = \"\"\n(([ ]|\S)*\n){"+str(cont)+"}\s{4}mac = \"(\S*)\"", answer[1])
                cont += 1
            for i in re_result:
                if i[3] == answer[2]:
                    interface.append("vif")
                    interface.append(str(i[0]))
                    interface.append(".0")
                    interface = "".join(interface)
                    break
            if not interface:
                raise CustomError("The program did not match the mac_destination in xenstore-ls /local/domain/0")
        else:
            interface = PHYSICALINTERFACE

        if request.type == "htb":
            configure_htb(request, interface)
        elif request.type == "psp":
            configure_psp(request, interface)
        elif request.type == "pspht":
            configure_pspht(request, interface)
        else:
            raise CustomError("Type " + request.type + " not supported")

    elif request.action == "deallocate":
        answer = same_host(request)
        if answer[0]:
            interface = []
            # if answer[0] is True, answer[1] contains the output of xenstore-ls and answer[2] contains the mac
            re_result = ""
            cont = 1
            while not re_result:
                re_result = re.findall("\n  ([0-9]*) = \"\"\n(([ ]|\S)*\n){"+str(cont)+"}\s{4}mac = \"(\S*)\"", answer[1])
                cont += 1
            for i in re_result:
                if i[3] == answer[2]:
                    interface.append("vif")
                    interface.append(str(i[0]))
                    interface.append(".0")
                    interface = "".join(interface)
                    break
            if not interface:
                raise CustomError("The program did not match the mac_destination in xenstore-ls /local/domain/0")
        else:
            interface = PHYSICALINTERFACE

        # TODO: Analisar requisição deallocate (separar por type (htb, pspht, psp)?)
        deallocate(request, interface)

    elif request.action == "clearall":
        p1 = subprocess.Popen(["ip", "link"], stdout=subprocess.PIPE)
        p1_output = p1.stdout.read()
        re_result = re.findall("p10p1|eth[0-9]|vif[0-9]*\.[0-9]*", p1_output)
        for i in re_result:
            del_qdisc(i)

    elif request.action == "check":
        p1 = subprocess.Popen(["ip", "link"], stdout=subprocess.PIPE)
        p1_output = p1.stdout.read()
        re_result = re.findall("p10p1|eth[0-9]|vif[0-9]*\.[0-9]*", p1_output)
        # response = "\n".join()
        for i in re_result:
            p1 = subprocess.Popen(["./check_interface.sh", i], stdout=subprocess.PIPE)
            response.append("--" + i + "\n")
            response.append(p1.stdout.read())
            response.append("\n")
    return "".join(response)

if __name__ == "__main__":
    # Check if the user is root
    if os.getuid() != 0:
        logging.error("You must be sudo")
        exit(1)

    # Check the default language of the system
    p1 = subprocess.Popen(["locale"], stdout=subprocess.PIPE)
    p1_output = p1.stdout.read()
    re_result = re.search("LANG=([a-z]*)", p1_output)
    if not re_result:
        logging.debug("Language set as default (English)")
        LANG = "en"
    else:
        LANG = re_result.group(1)

    # Avoid wait time during get_network_capacity of a PHYSICALINTERFACE
    #LSHW = subprocess.Popen(["lshw", "-C", "network"], stdout=subprocess.PIPE)
    PHYCAPACITY = get_phy_network_capacity()

    # Create a TCP/IP socket
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_sock.bind(("0.0.0.0", PORT))

    # Listen for a Request Manager (RM)
    server_sock.listen(1)

    while True:
        try:
            # Wait the connection
            logging.info("Waiting connection")
            client_sock, client_address = server_sock.accept()

            # Receive request
            data = client_sock.recv(BUFFERSIZE)

            # If no data received from Request Manager (RM)
            if not data:
                client_sock.sendall("No data received")
                client_sock.close()
                raise CustomError("No data received")

            # Create a request
            request = create_request(data)

            # Configure the request
            response = handle_request(request)

            # Send a response to the Request Manager (RM)
            client_sock.sendall(response)
            client_sock.close()
        except KeyboardInterrupt:
            logging.info("Bye")
            logging.debug(traceback.format_exc())
            client_sock.close()
            server_sock.close()
            exit(0)
        except CustomError:
            logging.error(sys.exc_value)
            logging.debug(traceback.format_exc())
        except Exception:
            logging.error("Something went wrong. See " + LOG_FILENAME + " file for more information")
            logging.debug(traceback.format_exc())
        finally:
            try:
                client_sock.close()
            except NameError:
                exit(0)
