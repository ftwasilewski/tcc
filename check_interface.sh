#!/bin/bash

tc -s -d qdisc show dev $1
tc -s -d class show dev $1
tc -s -d filter show dev $1
exit
